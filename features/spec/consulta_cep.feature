#utf-8
#language: pt

Funcionalidade: Validação de CEP

  @consulta_cep
  Cenário: Validar ibge do cep 
    Dado que busco por um CEP '03456040'
    Então valido o ibge

  @consulta_cep_invalido
  Cenário: Validar cep inválido 
    Dado que busco por um CEP '03465897'
    Então valido que o cep esta inválido